<!-- Trending Video Area Start -->
<section class="razo-trending-video-area section-padding-80-0 mb-50">
    <div class="container">
        <div class="row">
            <!-- Section Heading -->
            <div class="col-12">
                <div class="section-heading text-center">
                    <h2>Trending Video</h2>
                </div>
            </div>
        </div>

        <div class="row">
            <!-- Featured Trending Video -->
            <div class="col-12">
                <div class="featured-trending-video mb-30 wow fadeInUp" data-wow-delay="100ms">
                    <iframe src="https://www.youtube.com/embed/zRvhQ5Rf6-U" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
            </div>

            <!-- Single Post Area -->
            <div class="col-12 col-md-6 col-lg-4">
                <div class="razo-single-post d-flex mb-30 wow fadeInUp" data-wow-delay="100ms">
                    <!-- Post Thumbnail -->
                    <div class="post-thumbnail">
                        <a href="single-blog.html"><img src="img/bg-img/2.jpg" alt=""></a>
                    </div>
                    <!-- Post Content -->
                    <div class="post-content">
                        <div class="post-meta">
                            <a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i> 2.1k</a>
                            <a href="#"><i class="fa fa-eye" aria-hidden="true"></i> 3.6k</a>
                        </div>
                        <a href="single-blog.html" class="post-title">Epileptic boy's cannabis let through border</a>
                    </div>
                </div>
            </div>

            <!-- Single Post Area -->
            <div class="col-12 col-md-6 col-lg-4">
                <div class="razo-single-post d-flex mb-30 wow fadeInUp" data-wow-delay="300ms">
                    <!-- Post Thumbnail -->
                    <div class="post-thumbnail">
                        <a href="single-blog.html"><img src="img/bg-img/3.jpg" alt=""></a>
                    </div>
                    <!-- Post Content -->
                    <div class="post-content">
                        <div class="post-meta">
                            <a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i> 2.1k</a>
                            <a href="#"><i class="fa fa-eye" aria-hidden="true"></i> 3.6k</a>
                        </div>
                        <a href="single-blog.html" class="post-title">Ben Affleck completes addic treatment</a>
                    </div>
                </div>
            </div>

            <!-- Single Post Area -->
            <div class="col-12 col-md-6 col-lg-4">
                <div class="razo-single-post d-flex mb-30 wow fadeInUp" data-wow-delay="500ms">
                    <!-- Post Thumbnail -->
                    <div class="post-thumbnail">
                        <a href="single-blog.html"><img src="img/bg-img/4.jpg" alt=""></a>
                    </div>
                    <!-- Post Content -->
                    <div class="post-content">
                        <div class="post-meta">
                            <a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i> 2.1k</a>
                            <a href="#"><i class="fa fa-eye" aria-hidden="true"></i> 3.6k</a>
                        </div>
                        <a href="single-blog.html" class="post-title">Boaters have close call with humpback whale</a>
                    </div>
                </div>
            </div>

            <!-- Single Post Area -->
            <div class="col-12 col-md-6 col-lg-4">
                <div class="razo-single-post d-flex mb-30 wow fadeInUp" data-wow-delay="700ms">
                    <!-- Post Thumbnail -->
                    <div class="post-thumbnail">
                        <a href="single-blog.html"><img src="img/bg-img/5.jpg" alt=""></a>
                    </div>
                    <!-- Post Content -->
                    <div class="post-content">
                        <div class="post-meta">
                            <a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i> 2.1k</a>
                            <a href="#"><i class="fa fa-eye" aria-hidden="true"></i> 3.6k</a>
                        </div>
                        <a href="single-blog.html" class="post-title">Suspect in Mollie Tibbetts' killing gave false</a>
                    </div>
                </div>
            </div>

            <!-- Single Post Area -->
            <div class="col-12 col-md-6 col-lg-4">
                <div class="razo-single-post d-flex mb-30 wow fadeInUp" data-wow-delay="900ms">
                    <!-- Post Thumbnail -->
                    <div class="post-thumbnail">
                        <a href="single-blog.html"><img src="img/bg-img/6.jpg" alt=""></a>
                    </div>
                    <!-- Post Content -->
                    <div class="post-content">
                        <div class="post-meta">
                            <a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i> 2.1k</a>
                            <a href="#"><i class="fa fa-eye" aria-hidden="true"></i> 3.6k</a>
                        </div>
                        <a href="single-blog.html" class="post-title">Some of Mollie Tibbetts' family don't want</a>
                    </div>
                </div>
            </div>

            <!-- Single Post Area -->
            <div class="col-12 col-md-6 col-lg-4">
                <div class="razo-single-post d-flex mb-30 wow fadeInUp" data-wow-delay="1200ms">
                    <!-- Post Thumbnail -->
                    <div class="post-thumbnail">
                        <a href="single-blog.html"><img src="img/bg-img/7.jpg" alt=""></a>
                    </div>
                    <!-- Post Content -->
                    <div class="post-content">
                        <div class="post-meta">
                            <a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i> 2.1k</a>
                            <a href="#"><i class="fa fa-eye" aria-hidden="true"></i> 3.6k</a>
                        </div>
                        <a href="single-blog.html" class="post-title">Trump: Impeach somebody who's done great job?</a>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>
<!-- Trending Video Area End -->