Blog Area Start -->
<section class="razo-blog-area section-padding-80-0">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-8">
                <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                <!-- Banner ads -->
                <ins class="adsbygoogle"
                     style="display:inline-block;width:970px;height:90px"
                     data-ad-client="ca-pub-2095774043541922"
                     data-ad-slot="8947744209"></ins>
                <script>
                     (adsbygoogle = window.adsbygoogle || []).push({});
                </script>
            </div>
        </div>
        <div class="row">
            <!-- Weekly News Area -->
            <div class="col-12 col-md-8">
                <div class="weekly-news-area mb-50">
                    <!-- Section Heading -->
                    <div class="section-heading">
                        <h2>Blog News</h2>
                        {{-- Added new search func --}}
                        <div class="gcse-searchbox-only"></div>
                    </div>
                    <div id="feature-article"></div>
                    <div class="row" id="load-all-articles"></div>
                    <div class="row" id="pagination"></div>
                </div>
            </div>

            <!-- Trending News Area -->
            <div class="col-12 col-md-4">
                <div class="trending-news-area mb-50">

                    <!-- Section Heading -->
                    <div class="section-heading">
                        <h2>Trending</h2>
                    </div>

                    <div id="load-feature-trending-news"></div>
                    <div id="load-trending-news"></div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Blog Area End