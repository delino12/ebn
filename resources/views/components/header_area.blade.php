<header class="header-area">
    <!-- Main Header Start -->
    <div class="main-header-area">
        <div class="classy-nav-container breakpoint-off">
            <div class="container">
                <!-- Classy Menu -->
                <nav class="classy-navbar justify-content-between" id="razoNav">

                    <!-- Logo -->
                    <a class="nav-brand" href="{{ url('/') }}"><img src="{{asset('img/core-img/ebn-logo.png')}}" width="92" alt=""></a>

                    <!-- Navbar Toggler -->
                    <div class="classy-navbar-toggler">
                        <span class="navbarToggler"><span></span><span></span><span></span></span>
                    </div>

                    <!-- Menu -->
                    <div class="classy-menu">
                        <!-- Menu Close Button -->
                        <div class="classycloseIcon">
                            <div class="cross-wrap"><span class="top"></span><span class="bottom"></span></div>
                        </div>

                        <!-- Nav Start -->
                        <div class="classynav">
                            <ul id="nav">
                                <li><a href="{{url('/')}}">Home</a></li>
                                @if(Auth::check())
                                    <li><a href="{{url('/home')}}">Dashboard</a></li>
                                @endif
                                {{-- <li><a href="#">Trending</a>
                                    <ul class="dropdown">
                                        <li><a href="{{url('charts')}}">- Charts</a></li>
                                        <li><a href="{{url('charts')}}">- Charts Details</a></li>
                                        <li><a href="{{url('charts')}}">- Podcast</a></li>
                                        <li><a href="{{url('charts')}}">- Podcast Details</a></li>
                                        <li><a href="{{url('shows')}}">- Show</a></li>
                                        <li><a href="{{url('events')}}">- Event</a></li>
                                        <li><a href="{{url('articles')}}">- Blog</a></li>
                                        <li><a href="{{url('article')}}">- Blog Details</a></li>
                                    </ul>
                                </li> --}}
                                <li><a href="{{url('shows')}}">Shows</a></li>
                                <li><a href="{{url('lyrics')}}">lyrics</a></li>
                                <li><a href="{{url('musics')}}">Musics</a></li>
                                <li><a href="{{url('events')}}">Events</a></li>
                                <li><a href="{{url('articles')}}">Blog</a></li>

                                @if(Auth::check())
                                    <li><a href="javascript:void(0);"> | </a></li>
                                    <li><a href="{{url('logout')}}">Logout</a></li>
                                    <li><a href="{{url('profile')}}">
                                        <i class="fa fa-user"></i> {{ Auth::user()->name }}
                                    </a></li>
                                @else
                                    <li><a href="javascript:void(0);"> | </a></li>
                                    <li><a href="{{url('login')}}">Login</a></li>
                                    <li><a href="{{url('register')}}">Register</a></li>
                                @endif
                            </ul>

                            <!-- Share Icon -->
                            <div class="social-share-icon">
                                <i class="social_share"></i>
                            </div>

                            <!-- Search Icon -->
                            <div class="search-icon" data-toggle="modal" data-target="#searchModal">
                                <i class="icon_search"></i>
                            </div>
                        </div>
                        <!-- Nav End -->
                    </div>
                </nav>
            </div>
        </div>
    </div>
</header>
<!-- Header Area End