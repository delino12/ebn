@extends('layouts.admin-skin')

@section('title')
    Dashboard
@endsection

@section('contents')
	<div class="container">
		<div class="row p-20">
			<div class="col-6">
				<div class="card" style="box-shadow: 0rem 0rem 0.8rem 0rem rgba(0,0,0,0.50);">
					<div class="card-body">
						<div style="height: 300px;">
							
						</div>
					</div>
				</div>
			</div>
			<div class="col-6">
				<div class="card" style="box-shadow: 0rem 0rem 0.8rem 0rem rgba(0,0,0,0.50);">
					<div class="card-body">
						<div style="height: 300px;">
							
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="row p-20">
			<div class="col-10">
				<div class="card" style="box-shadow: 0rem 0rem 0.8rem 0rem rgba(0,0,0,0.50);">
					<div class="card-body">
						<div style="height: 300px;">
							
						</div>
					</div>
				</div>
			</div>
			<div class="col-2">
				<div class="card" style="box-shadow: 0rem 0rem 0.8rem 0rem rgba(0,0,0,0.50);">
					<div class="card-body">
						<div style="height: 300px;">
							
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="row p-20">
			<div class="col-12">
				<div class="card" style="box-shadow: 0rem 0rem 0.8rem 0rem rgba(0,0,0,0.50);">
					<div class="card-body">
						<div style="height: 300px;">
							
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="row p-20">
			<div class="col-12">
				<div class="card" style="box-shadow: 0rem 0rem 0.8rem 0rem rgba(0,0,0,0.50);">
					<div class="card-body">
						<div style="height: 300px;">
							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('scripts')
    <script type="text/javascript">
        
    </script>
@endsection