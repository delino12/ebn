@extends('layouts.web-skin')

@section('title')
    Lyrics
@endsection

@section('contents')
	<!-- Single Razo Event Area -->
    <div class="container">
        <div class="row section-padding-80">
            <div class="col-md-12">
                <form onsubmit="return searchLyricsDatabase()">
                    <div class="row">
                        <div class="form-group col-sm-10">
                            <input type="text" class="form-control" placeholder="Eg, Eminem Lyrics, J Cole Lyrics" id="keywords" name="">
                        </div>
                        <div class="form-group col-sm-2">
                            <button class="btn btn-razo btn-primary" type="submit">
                                <i class="fa fa-search"></i>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <div class="row section-padding-80">
            <div class="col-md-12" id="search-results"></div>
        </div>
    </div>

@endsection

@section('scripts')
    <script type="text/javascript">
        function searchLyricsDatabase() {
            var keywords = "Eminem Stan";
            var artist = keywords[0];
            var song = keywords[1];
            var query = {
                artist: artist,
                song: song
            }

            fetch('https://sridurgayadav-chart-lyrics-v1.p.rapidapi.com/apiv1.asmx/SearchLyricDirect', {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'X-RapidAPI-Host': 'sridurgayadav-chart-lyrics-v1.p.rapidapi.com',
                    'X-RapidAPI-Key': 'd791aee083msh80400db9ca623b4p194c82jsn64602fd48536'
                },
                body: JSON.stringify(query)
            }).then(r => {
                return r.json();
            }).then(results => {
                console.log(results)
            }).catch(err => {
                console.log(JSON.stringify(err));
            })

            // void form
            return false;
        }
    </script>
@endsection