<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Title -->
    <title>{{ env("APP_NAME") }} | @yield('title')</title>

    <meta name="description" content="EBN (Ekpoto Blog Network) is a platform that aim to show inside Africa, Entertainment, Lifestyle and News. Showing the world the other side of the picture and uncover hidden stories is always challenging but we are ready to bring sunrise of Africa to the rest of the world." />
    <meta name="keywords" content="Blog, Entertainment, Musics, Arts, Videos, Fashion, Lifestyle, Latest, Trending, News, Headlines, Technology, Twitter, Google, Facebook, Youtube" />
    <meta name="author" content="ekpotoliberty.com" />
    <meta property="fb:app_id" content="1294111604081869" />
    <meta property="og:title" content="EBN, No.1 Africa Entertainment Network"/>
    <meta property="og:image" content="{{env("APP_URL")}}/img/featured-img/1.jpg"/>
    <meta property="og:url" content="{{env("APP_URL")}}"/>
    <meta property="og:site_name" content="{{env("APP_NAME")}}"/>
    <meta property="og:type" content="website"/>
    <meta property="og:description" content="EBN (Ekpoto Blog Network) is a platform that aim to show inside Africa, Entertainment, Lifestyle and News. Showing the world the other side of the picture and uncover hidden stories is always challenging but we are ready to bring sunrise of Africa to the rest of the world."/>
    <meta name="google-site-verification" content="zB2aBYp1ukVakb5A50jRfshl30aqJViMPztPuVU4qkU" />

    {{-- Twitter Section --}}
    <meta name="twitter:title" content="EBN, No.1 Africa Entertainment Network" />
    <meta name="twitter:image" content="{{env("APP_URL")}}/img/featured-img/1.jpg" />
    <meta name="twitter:url" content="{{env("APP_URL")}}" />
    <meta name="twitter:card" content="EBN (Ekpoto Blog Network) is a platform that aim to show inside Africa, Entertainment, Lifestyle and News. Showing the world the other side of the picture and uncover hidden stories is always challenging but we are ready to bring sunrise of Africa to the rest of the world." />

    <meta name="propeller" content="e8638bf841bf05cc5d3b96e15183fb35">

    {{-- Material icons --}}
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700|Oswald:200,300,400,500,600,700">

    {{-- Cookie Policy --}}
    <link rel="stylesheet" type="text/css" href="//wpcc.io/lib/1.0.2/cookieconsent.min.css"/>
    <link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/animate.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/default-assets/classy-nav.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/default-assets/audioplayer.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/owl.carousel.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/magnific-popup.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/font-awesome.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}">


    <link rel="apple-touch-icon" sizes="57x57" href="img/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="img/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="img/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="img/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="img/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="img/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="img/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="img/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="img/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="img/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="img/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="img/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="img/favicon-16x16.png">
    <link rel="manifest" href="manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="img/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <!-- google -->
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-143698758-1"></script>
    {{-- Added google search engine --}}
    {{-- <script async src='https://cse.google.com/cse.js?cx=partner-pub-2095774043541922:6870139841'></script> --}}
    <script data-ad-client="ca-pub-2541464520474254" async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        
        gtag('config', 'UA-143698758-1');
    </script>

    <style type="text/css">
        /* On screens that are 992px or less, set the background color to blue */
        .notification-holder {
            height: 68px;
            background-color: #FFF;
            color: #333;
            text-align:center; 
            padding: 1rem;
        }

        @media screen and (max-width: 768px) {
            .notification-holder span, a {
                font-size: 13px;
                text-align: center;
            }
        }
    </style>
</head>

<body>
    <input type="hidden" id="token" value="{{ csrf_token() }}" name="">
    <div id="notification-wrapper"></div>


    <!-- Top Search Area -->
    @include('components.top_search_area')

    <!-- Social Share Area -->
    @include('components.social_share_area')

    <!-- Header Area -->
    @include('components.header_area')

    <!-- Yield Body -->
    @yield('contents')

    <!-- Footer -->
    @include('components.footer_area')

    <!-- All JS Files -->

    <!-- jQuery -->
    <script src="{{asset('js/jquery.min.js')}}"></script>
    <!-- Popper -->
    <script src="{{asset('js/popper.min.js')}}"></script>
    <!-- Bootstrap -->
    <script src="{{asset('js/bootstrap.min.js')}}"></script>
    <!-- All Plugins -->
    <script src="{{asset('js/razo.bundle.js')}}"></script>
    <!-- Active -->
    <script src="{{asset('js/default-assets/active.js')}}"></script>
    <script src="{{asset('js/sweetalert.min.js')}}"></script>
    <!-- Base64 min -->
    <script src="{{asset('js/base64.js')}}"></script>
    <!-- App Js -->
{{--     <script src="{{asset('js/app.js')}}"></script> --}}
    
    {{-- cookie policy --}}
    <script src="//wpcc.io/lib/1.0.2/cookieconsent.min.js"></script>
    <script>window.addEventListener("load", function(){window.wpcc.init({"colors":{"popup":{"background":"#f0edff","text":"#000000","border":"#5e65c2"},"button":{"background":"#5e65c2","text":"#ffffff"}},"position":"bottom","transparency":"25","content":{"href":"ebn.ng","message":"Welcome to EBN Network, this website uses cookies to ensure you get the best experience on our website.","link":"www.ebn.ng/policy"}})});</script>
    
    <!-- facebook -->
    <script async defer src="https://connect.facebook.net/en_US/sdk.js"></script>
    <script src="https://js.pusher.com/5.0/pusher.min.js"></script>
    <script type="text/javascript">
        var isPushEnabled = false;
        Pusher.logToConsole = true;
        var pusher = new Pusher('{{env('PUSHER_APP_KEY')}}', {encrypted: false, cluster: "eu"});
        var channel = pusher.subscribe('notify-article');
        channel.bind('EBN\\Events\\NotifyNewArticle', function(data){
            // console.log(data);
            // alert('new push notification');
            displayNotification(data);
        });

        window.fbAsyncInit = function() {
            FB.init({
                appId      : '1294111604081869',
                cookie     : true,
                xfbml      : true,
                version    : 'v3.3'
            });  
            FB.AppEvents.logPageView();
        };

        $(document).ready(function() {
            animatePushNotificationOption();
            // registerAServicesWorkerProcess();
        });

        (function(d, s, id){
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {return;}
            js = d.createElement(s); js.id = id;
            js.src = "https://connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));

        function animatePushNotificationOption() {
            $("#notification-wrapper").html(`
                <div id="load-push-notifications" class="row">
                    <div class="notification-holder text-center">
                        <span>
                            Turn on notification to get latest update on EBN media.
                            <span class="ml-20">
                                <a href="javascript:void(0);" onclick="enableNotfications()" class="btn-link js-push-button">
                                    click here
                                </a>
                            </span>
                        </span>
                    </div>
                </div>
            `).fadeIn(500, function() {
                console.log('Notification elemented added!');
            });
        }

        function enableNotfications() {
            if (isPushEnabled) {
              unsubscribe();
            } else {
              subscribe();
            }
        }

        function displayNotification(data) {
            if (Notification.permission == 'granted') {
                navigator.serviceWorker.getRegistration().then(function(reg) {
                    var options = {
                        body: data.message,
                        icon: data.avatar,
                        vibrate: [100, 50, 100],
                        data: {
                            dateOfArrival: Date.now(),
                            primaryKey: data.id
                        }
                    };
                    reg.showNotification(data.title, options);
                });
            }
        }

        window.addEventListener('load', function(){
            if ('serviceWorker' in navigator) {
                navigator.serviceWorker.register('/service-worker.js').then(initialiseState);
            } else {
                console.warn('Service workers aren\'t supported in this browser.');
            }
        });

        // init page and register services worker
        if(navigator.serviceWorker){
            // listen for controller change
            navigator.serviceWorker.addEventListener('controllerchange', function (){
                console.log('There is a change is government, the new services worker is taking over');
            });
        }else{
            console.log('browser does not support Services Worker !');
        }

        // Once the service worker is registered set the initial state
        function initialiseState(sw) {
            if(sw.waiting){
                console.log('there is a waiting service worker...');
            }

            if(sw.installing){
                console.log('there is a service worker installing...');
            }

            if(sw.active){
                console.log('there is an active services worker ');
            }

            if(sw){
                sw.addEventListener('stateChange', function(event){
                    console.log('there is a new service worker process');
                    console.log(event.target.state);
                })
            }

            // Are Notifications supported in the service worker?
            if (!('showNotification' in ServiceWorkerRegistration.prototype)) {
                console.warn('Notifications aren\'t supported.');
                return;
            }

            // Check the current Notification permission.
            // If its denied, it's a permanent block until the
            // user changes the permission
            if (Notification.permission === 'denied') {
                console.warn('The user has blocked notifications.');
                return;
            }

            // Check if push messaging is supported
            if (!('PushManager' in window)) {
                console.warn('Push messaging isn\'t supported.');
                return;
            }

            // We need the service worker registration to check for a subscription
            navigator.serviceWorker.ready.then(function(serviceWorkerRegistration) {
                // Do we already have a push message subscription?
                serviceWorkerRegistration.pushManager.getSubscription().then(function(subscription) {
                    // Enable any UI which subscribes / unsubscribes from
                    // push messages.
                    var pushButton = document.querySelector('.js-push-button');
                    pushButton.disabled = false;

                    if (!subscription) {
                        // We aren't subscribed to push, so set UI
                        // to allow the user to enable push
                        return;
                    }

                    console.log(subscription.endpoint);

                    // Keep your server in sync with the latest subscriptionId
                    // sendSubscriptionToServer(subscription);

                    // Set your UI to show they have subscribed for
                    // push messages
                    $("#notification-wrapper").hide();
                    pushButton.textContent = 'Disable Push Messages';
                    isPushEnabled = true;
                }).catch(function(err) {
                    console.warn('Error during getSubscription()', err);
                });
            });
        }

        function subscribe() {
            // Disable the button so it can't be changed while
            // we process the permission request
            var pushButton = document.querySelector('.js-push-button');
            pushButton.disabled = true;
            navigator.serviceWorker.ready.then(function(serviceWorkerRegistration) {
                serviceWorkerRegistration.pushManager.subscribe({userVisibleOnly: true}).then(function(subscription) {
                    // The subscription was successful
                    console.log(subscription.endpoint);
                    isPushEnabled = true;
                    pushButton.textContent = 'Disable Push Messages';
                    pushButton.disabled = false;

                    // TODO: Send the subscription.endpoint to your server
                    // and save it to send a push message at a later date
                    // return sendSubscriptionToServer(subscription);
                }).catch(function(err) {
                    console.warn(JSON.stringify(err))
                    if (Notification.permission === 'denied') {
                        // The user denied the notification permission which
                        // means we failed to subscribe and the user will need
                        // to manually change the notification permission to
                        // subscribe to push messages
                        console.warn('Permission for Notifications was denied');
                        // pushButton.disabled = true;
                    } else {
                        // A problem occurred with the subscription; common reasons
                        // include network errors, and lacking gcm_sender_id and/or
                        // gcm_user_visible_only in the manifest.
                        console.error('Unable to subscribe to push.', err);
                        // pushButton.disabled = false;
                        // pushButton.textContent = 'Enable Push Messages';
                    }
                });
            });
        }

        function unsubscribe() {
            var pushButton = document.querySelector('.js-push-button');
            pushButton.disabled = true;

            navigator.serviceWorker.ready.then(function(serviceWorkerRegistration) {
                // To unsubscribe from push messaging, you need get the
                // subscription object, which you can call unsubscribe() on.
                serviceWorkerRegistration.pushManager.getSubscription().then(function(pushSubscription) {
                    // Check we have a subscription to unsubscribe
                    if (!pushSubscription) {
                        // No subscription object, so set the state
                        // to allow the user to subscribe to push
                        isPushEnabled = false;
                        pushButton.disabled = false;
                        pushButton.textContent = 'Enable Push Messages';
                        return;
                    }

                    var subscriptionId = pushSubscription.subscriptionId;
                    // TODO: Make a request to your server to remove
                    // the subscriptionId from your data store so you
                    // don't attempt to send them push messages anymore

                    // We have a subscription, so call unsubscribe on it
                    pushSubscription.unsubscribe().then(function(successful) {
                        pushButton.disabled = false;
                        pushButton.textContent = 'Enable Push Messages';
                        isPushEnabled = false;
                    }).catch(function(e) {
                        // We failed to unsubscribe, this can lead to
                        // an unusual state, so may be best to remove
                        // the users data from your data store and
                        // inform the user that you have done so

                        console.warn('Unsubscription error: ', e);
                        pushButton.disabled = false;
                        pushButton.textContent = 'Enable Push Messages';
                    });
                }).catch(function(e) {
                    console.error('Error thrown while unsubscribing from push messaging.', e);
                });
            });
        }

        function urlBase64ToUint8Array(base64String) {
            const padding = '='.repeat((4 - base64String.length % 4) % 4);
            const base64 = (base64String + padding).replace(/-/g, '+').replace(/_/g, '/');

            const rawData = Base64.encode(base64);
            const outputArray = new Uint8Array(rawData.length);

            for (let i = 0; i < rawData.length; ++i) {
                outputArray[i] = rawData.charCodeAt(i);
            }
            return outputArray;
        }
    </script>
    @yield('scripts')
</body>
</html>