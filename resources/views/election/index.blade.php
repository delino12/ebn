@extends('layouts.web-skin')

@section('title')
    Election
@endsection

@section('contents')
	<div class="container-fluid section-padding-80">
		<div class="row">
			<div class="col-3">
				<div class="card" style="box-shadow: 0rem 0rem 0.8rem 0rem rgba(0,0,0,0.50);">
					<div class="card-body">
						<div style="height: 300px; background-image: url({{asset('img/bg-img/26.jpg')}});background-position: center; background-size: contain;">
							
						</div>
					</div>
				</div>
				<br />
			</div>
			<div class="col-3">
				<div class="card" style="box-shadow: 0rem 0rem 0.8rem 0rem rgba(0,0,0,0.50);">
					<div class="card-body">
						<div style="height: 300px;">
							
						</div>
					</div>
				</div>
				<br />
			</div>
			<div class="col-3">
				<div class="card" style="box-shadow: 0rem 0rem 0.8rem 0rem rgba(0,0,0,0.50);">
					<div class="card-body">
						<div style="height: 300px;">
							
						</div>
					</div>
				</div>
				<br />
			</div>
			<div class="col-3">
				<div class="card" style="box-shadow: 0rem 0rem 0.8rem 0rem rgba(0,0,0,0.50);">
					<div class="card-body">
						<div style="height: 300px;">
							
						</div>
					</div>
				</div>
				<br />
			</div>
			<div class="col-3">
				<div class="card" style="box-shadow: 0rem 0rem 0.8rem 0rem rgba(0,0,0,0.50);">
					<div class="card-body">
						<div style="height: 300px;">
							
						</div>
					</div>
				</div>
				<br />
			</div>
			<div class="col-3">
				<div class="card" style="box-shadow: 0rem 0rem 0.8rem 0rem rgba(0,0,0,0.50);">
					<div class="card-body">
						<div style="height: 300px;">
							
						</div>
					</div>
				</div>
				<br />
			</div>
			<div class="col-3">
				<div class="card" style="box-shadow: 0rem 0rem 0.8rem 0rem rgba(0,0,0,0.50);">
					<div class="card-body">
						<div style="height: 300px;">
							
						</div>
					</div>
				</div>
				<br />
			</div>
			<div class="col-3">
				<div class="card" style="box-shadow: 0rem 0rem 0.8rem 0rem rgba(0,0,0,0.50);">
					<div class="card-body">
						<div style="height: 300px;">
							
						</div>
					</div>
				</div>
				<br />
			</div>
		</div>
	</div>
@endsection

@section('scripts')
    <script type="text/javascript">
        
    </script>
@endsection