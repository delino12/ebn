@extends('layouts.web-skin')

@section('title')
    Election - Registration
@endsection

@section('contents')
	<div class="container-fluid section-padding-80">
		<div class="row">
			<div class="col-md-3"></div>
			<div class="col-md-6">
				<div class="card" style="box-shadow: 0rem 0rem 0.8rem 0rem rgba(0,0,0,0.50);">
					<div class="card-body">
						<form method="POST" onsubmit="return signupContestant()">
		                    <div class="form-group">
		                        <label for="contestant_election_id">Select Election</label>
		                        <select id="contestant_election_id" class="form-control" required="">
		                            <option value=""> Select Election </option>
		                        </select>
		                    </div>

		                    <div class="form-group">
		                        <label for="contestant_name">Contestant Names</label>
		                        <input type="text" id="contestant_name" class="form-control" placeholder="Eg John Wick" name="" required="">
		                    </div>

		                    <div class="row">
		                        <div class="col-6">
		                            <div class="form-group">
		                                <label for="contestant_department">Department</label>
		                                <input type="text" id="contestant_department" class="form-control" placeholder="Eg Accounting" name="">
		                            </div>
		                        </div>
		                        <div class="col-6">
		                            <div class="form-group">
		                                <label for="contestant_institution">Institution</label>
		                                <select id="contestant_institution" class="form-control">
		                                    <option value=""> Select Institution </option>
		                                    <option value="CRUTECH"> CRUTECH </option>
		                                </select>
		                            </div>
		                        </div>
		                    </div>

		                    <div class="row">
		                        <div class="col-6">
		                            <div class="form-group">
		                                <label for="contestant_state_of_origin">State of Origin</label>
		                                <select  id="contestant_state_of_origin" onchange="showLocalGovt()" class="form-control">
		                                    <option value=""> Select State of Origin</option>
		                                </select>
		                            </div> 
		                        </div>
		                        <div class="col-6">
		                            <div class="form-group">
		                                <label for="contestant_lga">LGA</label>
		                                <select id="contestant_lga" class="form-control">
		                                    <option value=""> Select LGA</option>
		                                </select>
		                            </div>
		                        </div>
		                    </div>

		                    <div class="row">
		                        <div class="col-6">
		                            <div class="form-group">
		                                <label for="contestant_gender">Gender</label>
		                                <select class="form-control" id="contestant_gender">
		                                    <option value=""> Select Gender </option>
		                                    <option value="1"> Female </option>
		                                    <option value="2"> Male </option>
		                                </select>
		                            </div>
		                        </div>
		                        <div class="col-6">
		                            <div class="form-group">
		                                <label for="contestant_age">Enter Age</label>
		                                <input type="number" placeholder="Eg. 19" step="any" min="15" class="form-control" id="contestant_age">
		                            </div>
		                        </div>
		                    </div>

		                    <div class="form-group">
		                        <label for="contestant_images">Images</label>
		                        <a href="javascript:void(0);" onclick="addImages()" class="btn btn-link">
		                            <i class="fa fa-camera"></i> Add
		                        </a>
		                    </div>

		                    <div class="images-addons"></div>
		                    <div class="images-preview"></div>

		                    <div class="form-group">
		                        <button type="submit" class="btn btn-primary">
		                            Create Contestant
		                        </button>
		                    </div>
						</form>
					</div>
				</div>
				<br />
			</div>
			<div class="col-md-3"></div>
		</div>
	</div>
@endsection

@section('scripts')
    <script type="text/javascript">
        loadAllElections();
    	fetchLocation();
		function addElection() {
			$("#add-election-modal").modal();		
		}

		function addCandidate() {
			$("#add-candidate-modal").modal();		
		}

		function createNewElection() {
			var _token = $("#token").val();
			var host = $("#election_host").val();
			var name = $("#election_name").val();
			var description = $("#election_description").val();
			var start_date = $("#election_start_date").val();
			var end_date = $("#election_end_date").val();
			var start_time = $("#election_start_time").val();
			var end_time = $("#election_end_time").val();

			var query = {_token, host, name, description, start_date, end_date, start_time, end_time}

			fetch(`{{url('add/new/election')}}`, {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json',
				},
				body: JSON.stringify(query)
			}).then(r => {
				return r.json();
			}).then(results => {
				// console.log(results);
				swal(
					results.status,
					results.message,
					results.status
				);
				loadAllElections();
				$("#add-election-modal").modal('hide');
			}).catch(err => {
				console.log(JSON.stringify(err));
			})

			// return void
			return false;
		}

		function createNewCandidate() {
			var _token 		= $("#token").val();
			var names 		= $("#contestant_name").val();
			var department 	= $("#contestant_department").val();
			var institution = $("#contestant_institution").val();
			var state_of_origin = $("#contestant_state_of_origin").val();
			var lga 		= $("#contestant_lga").val();
			var gender 		= $("#contestant_gender").val();
			var age 		= $("#contestant_age").val();
			var images      = $("#contestant_images").val();
			var election_id = $("#contestant_election_id").val();

			var query = {_token, names, department, institution, state_of_origin, lga, gender, age, images, election_id}

			fetch(`{{url('add/new/contestant')}}`, {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json',
				},
				body: JSON.stringify(query)
			}).then(r => {
				return r.json();
			}).then(results => {
				// console.log(results)
				// loadAllCandidates(election_idelection_id);
				swal(
					results.status,
					results.message,
					results.status
				);
			}).catch(err => {
				console.log(JSON.stringify(err));
			})

			// return void
			return false;
		}

		function loadAllElections() {
			fetch(`{{url('get/all/elections')}}`).then(r => {
				return r.json();
			}).then(results => {
				// console.log(results);
				var sn = 0;
				$("#load-elections").html("");
				$("#contestant_election_id").html("");
				$.each(results, function(index, val) {
					sn++;
					$("#load-elections").append(`
						<tr>
							<td>${sn}</td>
							<td>${val.name}</td>
							<td>${val.duration}</td>
							<td>${val.status}</td>
							<td>
								<a href="javascript:void(0);" onclick="viewMore(${val.id})" class="space-link">view</a>
							</td>
						</tr>
					`);

					$("#contestant_election_id").append(`
						<option value="${val.id}"> ${val.name} </option>
					`)
				});
			}).catch(err => {
				console.log(JSON.stringify(err));
			})
		}

		function loadAllCandidates(election_id) {
			fetch(`{{url('get/all/contestants')}}?election_id=${election_id}`).then(r => {
				return r.json();
			}).then(results => {
				// console.log(results);
				$("#load-candidates").html("");
				var sn = 0;
				$.each(results, function(index, val) {
					sn++;
					$("#load-candidates").append(`
						<tr>
							<td>${sn}</td>
							<td>${val.names}</td>
							<td>${val.total_votes}</td>
							<td>${val.gender}</td>
							<td>
								<a href="javascript:void(0);" onclick="viewCandidate(${val.id})" class="space-link">
									view
								</a>
							</td>
						</tr>
					`);
				});
			}).catch(err => {
				console.log(JSON.stringify(err));
			})
		}

		function fetchLocation() {
            fetch('{{url("database/location.json")}}').then(r => {
            	return r.json()
            }).then(data => {
                $("#contestant_state_of_origin").html('');
                $("#contestant_state_of_origin").append(`
                    <option value="">-- select state --</option>
                `);

                $.each(data, function(index, val) {
                    /* iterate through array or object */
                    $("#contestant_state_of_origin").append(`
                        <option value="${val.name}">${val.name}</option>
                    `);
                });
            	// $("#state_of_origin").select2();
            }).catch(err => {
              console.log(JSON.stringify(err));
            });
        }

        function showLocalGovt() {
            var state = $("#contestant_state_of_origin").val();

            fetch(`{{url('database/location.json')}}`).then(r => {
            	return r.json();
            }).then(results => {
            	// console.log(results)
            	$.each(results, function(index, val) {
                    if(state == val.name){
                        $("#contestant_lga").html("");
                        $.each(val.lga, function(index, val){
                            $("#contestant_lga").append(`
                                <option value="${val}">${val}</option>
                            `);
                        })
                    }
                });
                // $("#lga").select2();
            }).catch(err => {
            	console.log(JSON.stringify(err));
            })
        }

        function viewMore(election_id) {
        	// body...
        	loadAllCandidates(election_id);
        }
    </script>
@endsection