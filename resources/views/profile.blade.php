@extends('layouts.app')

@section('contents')
    <div class="container section-padding-50">
        <div class="row p-20">
            <div class="col-md-6">
                <div class="card" style="box-shadow: 0rem 0rem 0.8rem 0rem rgba(0,0,0,0.50);">
                    <div class="card-header">
                        Referral
                    </div>
                    <div class="card-body text-center">
                        <div style="min-height: 300px;">
                            <p id="show-referral-links"></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card" style="box-shadow: 0rem 0rem 0.8rem 0rem rgba(0,0,0,0.50);">
                    <div class="card-header">
                        Total Referrals
                    </div>
                    <div class="card-body text-center">
                        <div style="min-height: 300px;margin-top: 10px;">
                            <h1 id="total-referred-users"></h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row p-20">
            <div class="col-md-6">
                <div class="card" style="box-shadow: 0rem 0rem 0.8rem 0rem rgba(0,0,0,0.50);">
                    <div class="card-header">
                        Earnings
                    </div>
                    <div class="card-body">
                        <div style="min-height: 300px;">
                            <h3 class="text-center">
                                Account Balance
                            </h3>
                            <div class="text-center account-balance">loading...</div>

                            
                            <br />
                            <p>
                                <span class="text-danger">Note:</span>
                                Earning is track by a single-user comment on each article read. any other comment will not affect earnings.
                            </p>
                            <hr />
                            <div class="text-center">
                                <button class="btn btn-info" disabled="">
                                    <i class="fa fa-wallet"></i> Withdraw
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card" style="box-shadow: 0rem 0rem 0.8rem 0rem rgba(0,0,0,0.50);">
                    <div class="card-header">
                        Post/Article Engaged
                    </div>
                    <div class="card-body">
                        <div style="min-height: 300px;">
                            <h3 class="text-center">
                                Total Post Engaged
                            </h3>
                            <div class="text-center total-article-engaged">loading...</div>
                            <br />
                            <p>
                                <span class="text-danger">Note:</span>
                                This section shows <i>Articles</i> which has your comment count.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        // loadAllArticles();
        getReferralLink();
        getAccountBalance();
        getArticleEngaged();
        // getTotalReferredUsers();


        function getAccountBalance() {
            fetch(`{{url('get/client/balance')}}`).then(r => {
                return r.json();
            }).then(results => {
                console.log(results);
                $(".account-balance").html(`
                    <span style="font-size:50px;">&#8358;${results.account_bal}</span>
                `);
            }).catch(err => {
                console.log(JSON.stringify(err));
            })
        }

        function getArticleEngaged() {
            fetch(`{{url('get/article/engaged')}}`).then(r => {
                return r.json();
            }).then(results => {
                // console.log(results)
                $(".total-article-engaged").html(`
                    <span style="font-size:50px;">${results.total}</span>
                `);
            }).catch(err => {
                console.log(JSON.stringify(err));
            })
        }

        function getReferralLink() {
            fetch(`{{url('get/user/referral/link')}}`, {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                }
            }).then(r => {
                return r.json();
            }).then(results => {
                console.log(results);
                $("#show-referral-links").html(`
                    <a href="${results.url_link}">${results.url_link}</a>
                    <br /><br />
                    <a href="" class="btn btn-link"> <i class="fa fa-facebook"></i> facebook</a>
                    <a href="" class="btn btn-link"> <i class="fa fa-twitter"></i> twitter</a>
                    <a href="" class="btn btn-link"> <i class="fa fa-instagram"></i> instagram</a>
                    <a href="" class="btn btn-link"> <i class="fa fa-whatsapp"></i> whatsapp</a>
                `);

                $("#total-referred-users").html(`
                    ${results.total_referred}
                `);
            }).catch(err => {
                console.log(JSON.stringify(err));
            })
        }

        function getTotalReferredUsers() {
            fetch(`{{url('get/total/referred/users')}}`, {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                }
            }).then(r => {
                return r.json();
            }).then(results => {
                console.log(results)
                $("#total-referred-users").html(`
                    ${results.total}
                `);
            }).catch(err => {
                console.log(JSON.stringify(err));
            })
        }
    </script>
@endsection
