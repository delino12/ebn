@extends('layouts.app')

@section('contents')
    <div class="container section-padding-50">
        <div class="row p-20">
            <div class="col-md-6">
                <div class="card" style="box-shadow: 0rem 0rem 0.8rem 0rem rgba(0,0,0,0.50);">
                    <div class="card-header">
                        My Earnings
                    </div>
                    <div class="card-body">
                        <div style="min-height: 300px;">
                            <h3 class="text-center">
                                Account Balance
                            </h3>
                            <div class="text-center account-balance">loading...</div>

                            
                            <br />
                            <p>
                                <span class="text-danger">Note:</span>
                                Earning is track by a single-user comment on each article read. any other comment will not affect earnings.
                            </p>
                            <hr />
                            <div class="text-center">
                                <button class="btn btn-info" disabled="">
                                    <i class="fa fa-wallet"></i> Withdraw
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card" style="box-shadow: 0rem 0rem 0.8rem 0rem rgba(0,0,0,0.50);">
                    <div class="card-header">
                        Post/Article Engaged
                    </div>
                    <div class="card-body">
                        <div style="min-height: 300px;">
                            <h3 class="text-center">
                                Total Post Engaged
                            </h3>
                            <div class="text-center total-article-engaged">loading...</div>
                            <br />
                            <p>
                                <span class="text-danger">Note:</span>
                                This section shows <i>Articles</i> which has your comment count.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row p-20">
            <div class="col-12">
                <div class="card" style="box-shadow: 0rem 0rem 0.8rem 0rem rgba(0,0,0,0.50);">
                    <div class="card-header">
                        Transactions
                    </div>
                    <div class="card-body">
                        <div style="height: 300px;">
                            <p>You have not made any withdrawal yet!</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row p-20">
            <div class="col-12">
                <div class="card" style="box-shadow: 0rem 0rem 0.8rem 0rem rgba(0,0,0,0.50);">
                    <div class="card-header">
                        Disclaimer
                    </div>
                    <div class="card-body">
                        <p><b>USER EARN POLICY, TERMS AND CONDITIONS
</b></p><p><br></p><p>EBN (EKPOTO BLOG NETWORK), offers users who visit its site and views content an assuring avenue to invest and make an income not just by reading its contents but participating actively in its day to day progress by COMMENTS, OBSERVATIONS, and&nbsp; COMPLAINTS.</p><p>EBN offers a minimal interest for every CPA(comment per article), this enables users to generate while using the internet on Ebn website.</p><p>USER EARNINGS SHALL BE AT N2 PER COMMENT AND N50 PER REFERRAL&nbsp;</p><p><br></p><p>GAME TASKS SHALL COME UP ON THE&nbsp; Ebn website AT VARIOUS INTERVALS WHICH ATTRACTS N50 PER COMPLETED TASK.</p><p><br></p><p>TERMS AND CONDITIONS APPLY.</p><p><br></p><ol><li>1. USERS SHOULD REGISTER AND BE A MEMBER OF THE EBN NETWORK COMMUNITY</li></ol><ol><li>2. ALL AD BLOCK ON DEVICES MUST BE TURNED OFF</li></ol><ol><li>3. LOCATION IN DEVICE SHOULD BE ACTIVE</li></ol><ol><li>4. MULTIPLE ACCOUNTS ARE PROHIBITED</li></ol><ol><li>5. HATE SPEECH/FOUL WORDS ARE HIGHLY PROHIBITED</li></ol><ol><li>6. EARNINGS READ FOR ONLY CURRENT ARTICLES AS COMMENTS ON PREVIOUS ARTICLES SHALL NOT REFLECT</li></ol><ol><li>7. VIOLATION OF POLICY LEADS TO SUSPENSION OR TERMINATION OF ACCOUNTS.</li></ol><p><br></p><p>PAYMENT POLICY</p><p>Minimum withdrawal is rated at N3,000 for cash, and N1,000 for airtime from user earning account.</p><p>All withdrawal attract a 70/30 cut among user and Ebn respectively.</p><p><br></p><p>TERMS AND CONDITIONS OF POLICY CAN BE ADJUST BY EBN AT ANYTIME.</p>                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        // loadAllArticles();
        // loadTrendingArea();
        getAccountBalance();
        getArticleEngaged();

        function getAccountBalance() {
            fetch(`{{url('get/client/balance')}}`).then(r => {
                return r.json();
            }).then(results => {
                console.log(results);
                $(".account-balance").html(`
                    <span style="font-size:50px;">&#8358;${results.account_bal}</span>
                `);
            }).catch(err => {
                console.log(JSON.stringify(err));
            })
        }

        function getArticleEngaged() {
            fetch(`{{url('get/article/engaged')}}`).then(r => {
                return r.json();
            }).then(results => {
                // console.log(results)
                $(".total-article-engaged").html(`
                    <span style="font-size:50px;">${results.total}</span>
                `);
            }).catch(err => {
                console.log(JSON.stringify(err));
            })
        }

        function loadAllArticles(page = 0) {
            fetch(`{{url('clients/articles')}}/?page=${page}`, {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                }
            }).then(r => {
                return r.json();
            }).then(results => {
                $("#load-all-articles").html("");
                // console.log(results[0]);
                $.each(results.articles, function(index, val) {
                    $("#load-all-articles").append(`
                        <!-- Single Post Area -->
                        <div class="col-12 col-md-6">
                            <div class="razo-single-post d-flex mb-30">
                                <!-- Post Thumbnail -->
                                <div class="post-thumbnail">
                                    <a href="{{url('article')}}/${val.id}"><img src="${val.avatar}" alt=""></a>
                                </div>
                                <!-- Post Content -->
                                <div class="post-content">
                                    <div class="post-meta">
                                        
                                    </div>
                                    <a href="{{url('article')}}/${val.id}" class="post-title">${truncate(val.title, 30, '...')}</a>
                                </div>
                            </div>
                        </div>
                    `);
                });

                $("#pagination").html(`
                    <div class="col-md-12">
                        ${results.pagination}
                    </div>
                `);

                // show featured blog post
                $("#feature-article").html("");
                $("#feature-article").append(`
                    <!-- Featured Post Area -->
                    <div class="featured-post-area bg-img bg-overlay mb-30" style="background-image: url(${results[0].avatar});">
                        <!-- Post Overlay -->
                        <div class="post-overlay">
                            <div class="post-meta">
                                <a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i> 2.1k</a>
                                <a href="#"><i class="fa fa-eye" aria-hidden="true"></i> 3.6k</a>
                            </div>
                            <a href="{{url('article')}}/${results[0].id}" class="post-title">${results[0].title}</a>
                        </div>
                    </div>
                `);
            }).catch(err => {
                console.log(JSON.stringify(err));
            })
        }

        //executes code below when user click on pagination links
        $("#pagination").on( "click", ".list-inline a", function (e){
            e.preventDefault(); 
            var page = $(this).attr("data-page"); //get page number from link
            loadAllArticles(page);
        });

        function loadTrendingArea() {
            fetch("{{url('clients/trending/articles')}}", {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                }
            }).then(r => {
                return r.json();
            }).then(results => {
                // console.log(results[0]);
                $("#load-trending-news").html("");
                $.each(results, function(index, val) {
                    $("#load-trending-news").append(`
                        <!-- Single Post Area -->
                        <div class="razo-single-post d-flex mb-30">
                            <!-- Post Thumbnail -->
                            <div class="post-thumbnail">
                                <a href="{{url('article')}}/${val.id}"><img src="${val.avatar}" alt=""></a>
                            </div>
                            <!-- Post Content -->
                            <div class="post-content">
                                <div class="post-meta">
                                </div>
                                <a href="{{url('article')}}/${val.id}" class="post-title">
                                    ${truncate(val.title, 40, '...')}
                                </a>
                            </div>
                        </div>
                    `);
                });

                // show featured blog post
                $("#load-feature-trending-news").html("");
                $("#load-feature-trending-news").append(`
                    <!-- Featured Post Area -->
                    <div class="featured-post-area small-featured-post bg-img bg-overlay mb-30" style="background-image: url(${results[0].avatar});">
                        <!-- Post Overlay -->
                        <div class="post-overlay">
                            <div class="post-meta">
                            </div>
                            <a href="{{url('article')}}/${results[0].id}" class="post-title">
                                ${results[0].title}
                            </a>
                        </div>
                    </div>
                `);
            }).catch(err => {
                console.log(JSON.stringify(err));
            })
        }

        function truncate(string, length, delimiter) {
           delimiter = delimiter || "&hellip;";
           return string.length > length ? string.substr(0, length) + delimiter : string;
        };
    </script>
@endsection
