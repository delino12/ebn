<?php

namespace EBN;

use Illuminate\Database\Eloquent\Model;
use EBN\Election;
use EBN\Voter;

class Contestant extends Model
{
    /*
    |-----------------------------------------
    | ADD NEW CONTESTANT
    |-----------------------------------------
    */
    public function addNewContestant($payload){
    	// body
    	$already_exist = Contestant::where('names', $payload->names)->first();
    	if($already_exist == null){
    		$new_contestant 					= new Contestant();
    		$new_contestant->election_id        = $payload->election_id;
    		$new_contestant->names 				= $payload->names;
    		$new_contestant->department 		= $payload->department;
    		$new_contestant->institution 		= $payload->institution;
    		$new_contestant->state_of_origin 	= $payload->state_of_origin;
    		$new_contestant->lga 				= $payload->lga;
    		$new_contestant->gender 			= $payload->gender;
    		$new_contestant->age	 			= $payload->age;
    		$new_contestant->images 			= $payload->images; 
    		$new_contestant->status   			= 1;
    		if($new_contestant->save()){
    			$data = [
    				'status' 	=> 'success',
    				'message' 	=> 'Candidate created successfully!'
    			];
    		}else{
    			$data = [
    				'status' 	=> 'error',
    				'message' 	=> 'Error creating candidate, try again!'
    			];
    		}
    	}else{
    		$data = [
    			'status' 	=> 'error',
    			'message' 	=> $payload->names.' already exist!'
    		];
    	}

    	// return 
    	return $data;
    }

    /*
    |-----------------------------------------
    | GET ALL CONTESTANTS
    |-----------------------------------------
    */
    public function getAllContestants($payload){
    	// body
    	$contestants = Contestant::where("election_id", $payload->election_id)->orderBy('created_at', 'DESC')->get();
    	$contestants_box = [];
    	foreach ($contestants as $key => $value) {
    		if($value->gender == 1){
    			$value->gender = "Female";
    		}else{
    			$value->gender = "Male";
    		}

    		$data = [
    			'election_id'       => $value->election_id,
    			'names' 			=> $value->names,
    			'department' 		=> $value->department,
    			'institution' 		=> $value->institution,
    			'state_of_origin' 	=> $value->state_of_origin,
    			'lga' 				=> $value->lga,
    			'gender' 			=> $value->gender,
    			'age' 				=> $value->age,
    			'images' 			=> $value->images,
    			'status' 			=> $value->status,
    			'total_votes'       => 0
    		];
    		array_push($contestants_box, $data);
    	}

    	// return 
    	return $contestants_box;
    }
}
