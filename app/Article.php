<?php

namespace EBN;

use Illuminate\Database\Eloquent\Model;
use EBN\Events\NotifyNewArticle;
use EBN\Comment;
use EBN\ArticleSource;
use EBN\ArticleAuthor;
use Auth;

class Article extends Model
{
    /*
    |-----------------------------------------
    | RELATE WITH COMMENTS
    |-----------------------------------------
    */
    public function comments(){
        // body
        return $this->hasMany(Comment::class, 'article_id');
    }

    /*
    |-----------------------------------------
    | ADD NEW ARTICLE
    |-----------------------------------------
    */
    public function addNewArticle($payload){
    	// body
    	if(empty($payload->avatar)){
    		$payload->avatar = 'https://res.cloudinary.com/delino12/image/upload/v1562252126/resume/paklkhw4lutndzz6mfmo.jpg';
    	}
    	
    	$new_article 			= new Article();
    	$new_article->title 	= $payload->title;
    	$new_article->contents 	= $payload->contents;
    	$new_article->avatar 	= $payload->avatar;
    	$new_article->category 	= $payload->category;
    	$new_article->status 	= 1;
    	if($new_article->save()){

            $article_author             = new ArticleAuthor();
            $article_author->admin_id   = Auth::guard('admin')->user()->id;
            $article_author->article_id = $new_article->id;
            $article_author->save();

            $event_data = [
                'id'        => $new_article->id,
                'title'     => str_limit(Strip_tags($payload->title), 50, '....'),
                'message'   => str_limit(Strip_tags($payload->contents), 50, 'read more'),
                'image'     => $payload->avatar,
            ];

            event(new NotifyNewArticle($event_data));

    		$data = [
    			'status' 	=> 'success',
    			'message' 	=> 'Article saved!'
    		];
    	}else{
    		$data = [
    			'status' 	=> 'error',
    			'message' 	=> 'Failed to save article'
    		];
    	}

    	// data
    	return $data;
    }

    /*
    |-----------------------------------------
    | UPDATE ARTICLE
    |-----------------------------------------
    */
    public function updateAnArticle($payload){
        // body
        if(empty($payload->avatar)){
            $payload->avatar = 'https://res.cloudinary.com/delino12/image/upload/v1562252126/resume/paklkhw4lutndzz6mfmo.jpg';
        }
        
        $article = Article::find($payload->article_id);
        if($article !== null){
            $article->title     = $payload->title;
            $article->contents  = $payload->contents;
            $article->avatar    = $payload->avatar;
            $article->category  = $payload->category;
            $article->status    = 1;
            if($article->update()){
                $data = [
                    'status'    => 'success',
                    'message'   => 'Article updated!'
                ];
            }else{
                $data = [
                    'status'    => 'error',
                    'message'   => 'Failed to update article'
                ];
            }
        }else{
            $data = [
                'status'    => 'error',
                'message'   => 'Article does not exist!'
            ];
        }    

        // data
        return $data;
    }

    /*
    |-----------------------------------------
    | GET ALL ARTICLES
    |-----------------------------------------
    */
    public function getAllArticles($payload){
        // body
        $page_number = $payload->page;

        // total articles
        $total_article = Article::count();
        $per_page = 10;

        //break records into pages
        $total_pages = ceil($total_article/$per_page);

        //position of records
        $page_position = (($page_number - 1) * $per_page);

        // take and release
        $articles = Article::orderBy('created_at', 'DESC')->skip($page_position)->take($per_page)->get();

        $data = [
            'articles'      => $articles,
            'pagination'    => $this->paginate_function($per_page, $page_number, $total_article, $total_pages)
        ];

        // data
        return $data;
    }

    /*
    |-----------------------------------------
    | GET ALL THE ARTICLES ON CLIENT AREA
    |-----------------------------------------
    */
    public function getAllArticlesClients($payload){
        // body
        // body
        $page_number = $payload->page;

        // total articles
        $total_article = Article::count();
        $per_page = 20;

        //break records into pages
        $total_pages = ceil($total_article/$per_page);

        //position of records
        $page_position = (($page_number - 1) * $per_page);

        // take and release
        $articles = Article::orderBy('created_at', 'DESC')->skip($page_position)->take($per_page)->get();
        $filtered = [];
        foreach ($articles as $key => $value) {
            $article = [
                'id'            => $value->id,
                'title'         => $value->title,
                'link_title'    => str_replace(' ', '-', strip_tags($value->title)),
                'contents'      => $value->contents,
                'avatar'        => $value->avatar,
                'status'        => $value->status,
                'created_at'    => $value->created_at->isoFormat('dddd D Y'),
                'updated_at'    => $value->updated_at->diffForHumans(),
            ];

            array_push($filtered, $article);
        }

        $data = [
            'articles'      => $filtered,
            'pagination'    => $this->paginate_function($per_page, $page_number, $total_article, $total_pages)
        ];

        // data
        return $data;
    }

    /*
    |-----------------------------------------
    | GET ALL THE ARTICLES ON CLIENT AREA
    |-----------------------------------------
    */
    public function getClientArticleByTitle($article_title){
        // body
        $article = Article::where('title', $article_title)->first();

        // return 
        return $article;
    }

    /*
    |-----------------------------------------
    | GET SHUFFLED ARTICLES
    |-----------------------------------------
    */
    public function getTrendingArticle(){
        // body
        $articles = Article::orderBy('created_at', 'DESC')->limit('8')->get()->shuffle();
        $filtered = [];
        foreach ($articles as $key => $value) {
            $article = [
                'id'            => $value->id,
                'title'         => $value->title,
                'link_title'    => str_replace(' ', '-', $value->title),
                'contents'      => $value->contents,
                'avatar'        => $value->avatar,
                'status'        => $value->status,
                'created_at'    => $value->created_at->isoFormat('dddd D Y'),
                'updated_at'    => $value->updated_at->diffForHumans(),
            ];

            array_push($filtered, $article);
        }

        // data
        return $filtered;
    }

    /*
    |-----------------------------------------
    | GET RELATED ARTICLES 
    |-----------------------------------------
    */
    public function getArticleByRelation($tags){
        // body
        $articles = Article::orderBy('created_at', 'DESC')->limit('3')->get();
        $filtered = [];
        foreach ($articles as $key => $value) {
            $article = [
                'id'            => $value->id,
                'title'         => $value->title,
                'link_title'    => str_replace(' ', '-', $value->title),
                'contents'      => $value->contents,
                'avatar'        => $value->avatar,
                'status'        => $value->status,
                'created_at'    => $value->created_at->isoFormat('dddd D Y'),
                'updated_at'    => $value->updated_at->diffForHumans(),
            ];

            array_push($filtered, $article);
        }

        // data
        return $filtered;
    }

    /*
    |-----------------------------------------
    | DELETE ARTICLE
    |-----------------------------------------
    */
    public function delArticle($payload){
        // body
        $article = Article::find($payload->article_id);
        if($article !== null){
            if($article->delete()){
                $data = [
                    'status'    => 'success',
                    'message'   => 'Article Deleted!'
                ];
            }else{
                $data = [
                    'status'    => 'error',
                    'message'   => 'Failed to delete article!'
                ];
            }
        }else{
            $data = [
                'status'    => 'error',
                'message'   => 'Article not found!'
            ];
        }

        // return 
        return $data;
    }

    /*
    |-----------------------------------------
    | GET ONE ARTICLE
    |-----------------------------------------
    */
    public function getOneArticle($article_id){
        // body
        $article = Article::where('id', $article_id)->first();

        // return 
        return $article;
    }

    /*
    |-----------------------------------------
    | ADD REMOTE NEWS
    |-----------------------------------------
    */
    public function addRemoteNews($payload){
        // body
        $news_url   = "https://newsapi.org/v2/top-headlines";
        $api_key    = "7d8d32b47d9e4631824cfa3cb64702a7";
        $channel    = $payload->filter;
        $filter     = $payload->channel;

        switch ($filter) {
            case 1:
                # code...
                $endpoint = $news_url."?sources=$channel&apiKey=$api_key";
                break;
            
            case 9:
                # code...
                $endpoint = $news_url."?country=ng&category=business&apiKey=$api_key";
                break;

            default:
                # code...
                $endpoint = $news_url."?country=ng&category=business&apiKey=$api_key";
                break;
        }

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $endpoint);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 200);
        curl_setopt($ch, CURLOPT_TIMEOUT, 200);
        $res = curl_exec($ch);
        $data = json_decode($res, true);

        if($data !== null){
            $total = 0;
            foreach ($data['articles'] as $el) {
                if($el['title'] !== null && $el['description'] !== null && $el['urlToImage'] !== null){
                    $title      = str_replace("'", "", $el['title']);
                    $contents   = str_replace('"', '', $el['description']);
                    $avatar     = str_replace('http://', 'https://', $el['urlToImage']);
                    $source     = $el['source']['name'];
                    $author     = $el['author'];
                    $location   = "NG";

                    $new_article            = new Article();
                    $new_article->title     = $title;
                    $new_article->contents  = $contents;
                    $new_article->avatar    = $avatar;
                    $new_article->category  = 1;
                    $new_article->status    = 1;
                    if($new_article->save()){
                        $article_id = $new_article->id;

                        $article_source = new ArticleSource();
                        $article_source->addToSource($article_id, $source, $author, $location);

                        $total++;
                    }
                }
            }
            $data = [
                'status'    => 'error',
                'message'   => $total.' article was uploaded!'
            ];;
        }else{
            $data = [
                'status'    => 'error',
                'message'   => 'No news article was uploaded!'
            ];;
        }

        // return
        return $data;
    }

    /*
    |-----------------------------------------
    | PAGINATION SETTINGS
    |-----------------------------------------
    */
    public function paginate_function($item_per_page, $current_page, $total_records, $total_pages){
        // body
        $pagination = '';
        if($total_pages > 0 && $total_pages != 1 && $current_page <= $total_pages){ 
            //verify total pages and current page number
            $pagination .= '<ul class="list-inline pagination">';
           
            $right_links    = $current_page + 3;
            $previous       = $current_page - 3; //previous link
            $next           = $current_page + 1; //next link
            $first_link     = true; //boolean var to decide our first link
           
            if($current_page > 1){
                $previous_link = ($previous==0)?1:$previous;
                $pagination .= '<li><a href="#" data-page="1" title="First">First</a></li>'; //first link
                $pagination .= '<li><a href="#" data-page="'.$previous_link.'" title="Previous">Previous</a></li>'; //previous link
                    for($i = ($current_page-2); $i < $current_page; $i++){ //Create left-hand side links
                        if($i > 0){
                            $pagination .= '<li><a href="#" data-page="'.$i.'" title="Page'.$i.'">'.$i.'</a></li>';
                        }
                    }  
                $first_link = false; //set first link to false
            }
           
            if($first_link){ //if current active page is first link
                if($current_page == 0){
                    $pagination .= '';
                }else{
                   $pagination .= '<li class="li:first-child">'.$current_page.'</li>'; 
                }
            }elseif($current_page == $total_pages){ //if it's the last active link
                $pagination .= '<li class="li:last-child">'.$current_page.'</li>';
            }else{ //regular current link
                $pagination .= '<li>'.$current_page.'</li>';
            }
                   
            for($i = $current_page+1; $i < $right_links ; $i++){ //create right-hand side links
                if($i<=$total_pages){
                    $pagination .= '<li><a href="#" data-page="'.$i.'" title="Page '.$i.'">'.$i.'</a></li>';
                }
            }

            if($current_page < $total_pages){
                    $next_link = ($i > $total_pages)? $total_pages : $i;
                    $pagination .= '<li><a href="#" data-page="'.$next_link.'" title="Next">Next</a></li>'; //next link
                    $pagination .= '<li><a href="#" data-page="'.$total_pages.'" title="Last">Last</a></li>'; //last link
            }
           
            $pagination .= '</ul>';
        }

        return $pagination; //return pagination links
    }

    /*
    |-----------------------------------------
    | GET ARTICLE ENGAGEMENT
    |-----------------------------------------
    */
    public function articleEngagementCount($payload){
        // body
        $get_all_users_comments = Comment::where("user_id", $payload->user_id)->get();
        $comments_article_box = [];
        foreach ($get_all_users_comments as $key => $value) {
            $data = [
                "user_id" => $value->user_id,
                "article_id" => $value->article_id
            ];       

            if(!in_array($data, $comments_article_box)){
                array_push($comments_article_box, $data);
            }
        }

        $data = [
            'status'    => 'success',
            'total'     => count($comments_article_box)
        ];

        // return 
        return $data;
    }
}
