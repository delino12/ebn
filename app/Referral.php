<?php

namespace EBN;

use Illuminate\Database\Eloquent\Model;
use EBN\ReferredUser;
use EBN\User;
use EBN\Account;
use Auth;

class Referral extends Model
{
	/*
	|-----------------------------------------
	| REFFERED USERS
	|-----------------------------------------
	*/
	public function referred_users(){
		// body
		return $this->hasMany(ReferredUser::class);
	}

    /*
    |-----------------------------------------
    | ADD NEW REFERRAL ACCOUNT
    |-----------------------------------------
    */
    public function initReferral(){
    	// body
    	$referral_account = Referral::where('user_id', Auth::user()->id)->first();
    	if($referral_account == null){
    		// create user default referral
    		$new_referral_account 			= new Referral();
    		$new_referral_account->url_link = env("APP_URL").'/referral?ebn_referral_link=EBN-'.time();
    		$new_referral_account->user_id 	= Auth::user()->id;
    		$new_referral_account->save();  
    	}
    }

    /*
    |-----------------------------------------
    | GET REFERRAL ACCOUNT
    |-----------------------------------------
    */
    public function referralGetAccount($payload){
    	// body
    	$this->initReferral();
    	$referral_account = Referral::where('user_id', Auth::user()->id)->first();
    	$data = [
    		'id' 				=> $referral_account->id,
    		'user_id' 			=> $referral_account->user_id,
    		'url_link' 			=> $referral_account->url_link,
    		'referred_users' 	=> ReferredUser::where('referral_id', $referral_account->id)->with('users')->get(),
    		'total_referred' 	=> ReferredUser::where('referral_id', $referral_account->id)->count()
    	];

    	// return 
    	return $data;
    }
}
