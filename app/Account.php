<?php

namespace EBN;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Account extends Model
{
	/*
	|-----------------------------------------
	| ACCOUNT TO USER RELATIONSHIP
	|-----------------------------------------
	*/
	public function user(){
		// body
		return $this->belongsTo(User::class, "id");
	}

    /*
    |-----------------------------------------
    | CREATE DEFAULT ACCOUNT
    |-----------------------------------------
    */
    public function createUserDefaultAccount(){
    	// body
    	$user_id 		= Auth::user()->id;
    	$account_no 	= '09'.time();
    	$account_bal 	= 100.00; // default account balance

    	// check already exist
    	$already_exist = Account::where("user_id", $user_id)->first();
    	if($already_exist == null){
    		// create new account
    		$new_account 				= new Account();
    		$new_account->user_id 		= $user_id;
    		$new_account->account_no 	= $account_no;
    		$new_account->account_bal 	= $account_bal;
    		$new_account->status 		= true;
    		if($new_account->save()){
    			$data = [
    				'status' 	=> 'success',
    				'message' 	=> 'Account created!'
    			];
    		}else{
    			$data = [
    				'status' 	=> 'error',
    				'message' 	=> 'Error creating user account'
    			];
    		}
    	}else{
    		$data = [
    			'status' 	=> 'error',
    			'message' 	=> 'Account already exist!'
    		];
    	}

    	// return
    	return true;
    }

    /*
    |-----------------------------------------
    | CREDIT USER ACCOUNT
    |-----------------------------------------
    */
    public function credit($payload){
    	// body
    	$check_account = Account::where("user_id", $payload->user_id)->first();
    	if($check_account !== null){
    		// create new account
    		$account 				= Account::find($check_account->id);
    		$account->account_bal 	= $account->account_bal + $payload->amount;
    		if($account->update()){
    			$data = [
    				'status' 	=> 'success',
    				'message' 	=> 'Account has been credited!'
    			];
    		}else{
    			$data = [
    				'status' 	=> 'error',
    				'message' 	=> 'Error crediting user account'
    			];
    		}
    	}else{
    		$data = [
    			'status' 	=> 'error',
    			'message' 	=> 'No user account found!'
    		];
    	}

    	// return
    	return $data;
    }

    /*
    |-----------------------------------------
    | DEBIT USER ACCOUNT
    |-----------------------------------------
    */
    public function debit($payload){
    	// body
    	$check_account = Account::where("user_id", $payload->user_id)->first();
    	if($check_account !== null){
    		// create new account
    		$account 				= Account::find($check_account->id);
    		$account->account_bal 	= $account->account_bal - $payload->amount;
    		if($account->update()){
    			$data = [
    				'status' 	=> 'success',
    				'message' 	=> 'Account has been debited!'
    			];
    		}else{
    			$data = [
    				'status' 	=> 'error',
    				'message' 	=> 'Error debiting user account'
    			];
    		}
    	}else{
    		$data = [
    			'status' 	=> 'error',
    			'message' 	=> 'No user account found!'
    		];
    	}

    	// return
    	return $data;
    }

    /*
    |-----------------------------------------
    | LOCK USER ACCOUNT
    |-----------------------------------------
    */
    public function lock($payload){
    	// body
    	$check_account = Account::where("user_id", $payload->user_id)->first();
    	if($check_account !== null){
    		// create new account
    		$account 				= Account::find($check_account->id);
    		$account->status 		= false;
    		if($account->update()){
    			$data = [
    				'status' 	=> 'success',
    				'message' 	=> 'Account has been disabled!'
    			];
    		}else{
    			$data = [
    				'status' 	=> 'error',
    				'message' 	=> 'Error disabling user account'
    			];
    		}
    	}else{
    		$data = [
    			'status' 	=> 'error',
    			'message' 	=> 'No user account found!'
    		];
    	}

    	// return
    	return $data;
    }

    /*
    |-----------------------------------------
    | UNLOCK USER ACCOUNT
    |-----------------------------------------
    */
    public function unlock($payload){
    	// body
    	$check_account = Account::where("user_id", $payload->user_id)->first();
    	if($check_account !== null){
    		// create new account
    		$account 				= Account::find($check_account->id);
    		$account->status 		= true;
    		if($account->update()){
    			$data = [
    				'status' 	=> 'success',
    				'message' 	=> 'Account has been enabled!'
    			];
    		}else{
    			$data = [
    				'status' 	=> 'error',
    				'message' 	=> 'Error enabling user account'
    			];
    		}
    	}else{
    		$data = [
    			'status' 	=> 'error',
    			'message' 	=> 'No user account found!'
    		];
    	}

    	// return
    	return $data;
    }

    /*
    |-----------------------------------------
    | FETCH ALL ACCOUNT
    |-----------------------------------------
    */
    public function fetchAllAccount($payload){
    	// body
    	$all_account = Account::orderBy('created_at', 'DESC')->get();
    	$account_box = [];
    	foreach ($all_account as $key => $value) {
    		$account = [
    			'id' 			=> $value->id,
    			'account_bal' 	=> $value->account_bal,
    			'account_no' 	=> $value->account_no,
    			'status' 		=> $value->status,
    			'created_at' 	=> $value->created_at,
    		];

    		array_push($account_box, $account);
    	}

    	// return
    	return $account_box; 
    }

    /*
    |-----------------------------------------
    | FETCH ACCOUNT BY ID
    |-----------------------------------------
    */
    public function fetchAccountById($payload){
    	// body
    	$account = Account::where('id', $payload->id)->first();
    	if($account !== null) {
    		$account = [
    			'id' 			=> $value->id,
    			'account_bal' 	=> $value->account_bal,
    			'account_no' 	=> $value->account_no,
    			'status' 		=> $value->status,
    			'created_at' 	=> $value->created_at,
    		];
    		// array_push($account_box, $account);
    	}else{
    		$account = [];
    	}

    	// return
    	return $account; 
    }

    /*
    |-----------------------------------------
    | FETCH ACCOUNT BY USER ID
    |-----------------------------------------
    */
    public function fetchAccountByUserId($payload){
    	// body
        $this->createUserDefaultAccount(); // if account don't exist

    	$account = Account::where('user_id', $payload->user_id)->first();
    	if($account !== null) {
    		$account = [
    			'id' 			=> $account->id,
    			'account_bal' 	=> number_format($account->account_bal, 2),
    			'account_no' 	=> $account->account_no,
    			'status' 		=> $account->status,
    			'created_at' 	=> $account->created_at,
    		];
    		// array_push($account_box, $account);
    	}else{
    		$account = [];
    	}

    	// return
    	return $account; 
    }
}
