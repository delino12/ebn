<?php

namespace EBN;

use Illuminate\Database\Eloquent\Model;

class Election extends Model
{
    /*
    |-----------------------------------------
    | ADD NEW ELECTION
    |-----------------------------------------
    */
    public function addNewElection($payload){
    	// body
    	$already_exist = Election::where('name', $payload->name)->first();
    	if($already_exist == null){
    		$new_election 				= new Election();
    		$new_election->host 		= $payload->host;
    		$new_election->name 		= $payload->name;
    		$new_election->description 	= $payload->description;
    		$new_election->start_date 	= $payload->start_date;
    		$new_election->end_date 	= $payload->end_date;
    		$new_election->start_time 	= $payload->start_time;
    		$new_election->end_time	 	= $payload->end_time;
    		$new_election->status 		= false; 
    		if($new_election->save()){
    			$data = [
    				'status' 	=> 'success',
    				'message' 	=> 'Election created successfully!'
    			];
    		}else{
    			$data = [
    				'status' 	=> 'error',
    				'message' 	=> 'Error creating election, try again!'
    			];
    		}
    	}else{
    		$data = [
    			'status' 	=> 'error',
    			'message' 	=> $payload->name.' already exist!'
    		];
    	}

    	// return 
    	return $data;
    }

    /*
    |-----------------------------------------
    | GET ALL ELECTIONS
    |-----------------------------------------
    */
    public function getAllElections($payload){
    	// body
    	$elections = Election::orderBy('created_at', 'DESC')->get();
    	$elections_box = [];
    	foreach ($elections as $key => $value) {
    		if($value->status == false){
    			$value->status = '<span class="text-danger">Inactive</span>';
    		}else{
    			$value->status = '<span class="text-success">Active</span>';
    		}

    		$starting_date = explode('-', $value->start_date);
    		$ending_date = explode('-', $value->end_date);

    		$duration = $ending_date[2] - $starting_date[2];

    		$data = [
                'id'   => $value->id,
    			'host' => $value->host,
    			'name' => $value->name,
    			'description' => $value->description,
    			'start_date' => $value->start_date,
    			'end_date' => $value->end_date,
    			'start_time' => $value->start_time,
    			'end_time' => $value->end_time,
    			'status' => $value->status,
    			'duration' => $duration.' Days',
    		];

    		array_push($elections_box, $data);
    	}

    	// return 
    	return $elections_box;
    }
}
