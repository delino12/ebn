<?php

namespace EBN;

use Illuminate\Database\Eloquent\Model;

class Voter extends Model
{
	/*
	|-----------------------------------------
	| ADD NEW VOTER
	|-----------------------------------------
	*/
	public function addNewVoter($paylaod){
		// body
		$already_exist = Voter::where("phone", $paylaod->phone)->first();
		if($already_exist == null){
			$new_voter 				= new Voter();
			$new_voter->election_id = $paylaod->election_id;
			$new_voter->names 		= $paylaod->names;
			$new_voter->phone 		= $paylaod->phone;
			$new_voter->status 		= true;
			if($new_voter->save()){
				$data = [
					'status' 	=> 'success',
					'message' 	=> 'Voters created success'
				];
			}else{
				$data = [
					'status' 	=> 'error',
					'message' 	=> 'Error creating voter account'
				];
			}
		}else{
			$data = [
				'status' => 'error',
				'message' => $paylaod->phone.' already exist'
			];
		}

		// return
		return $data;
	}
}
