<?php

namespace EBN;

use Illuminate\Database\Eloquent\Model;
use EBN\User;
use EBN\Referral;

class ReferredUser extends Model
{
    /*
    |-----------------------------------------
    | USERS
    |-----------------------------------------
    */
    public function users(){
    	// body
    	return $this->belongsTo(User::class);
    }

    /*
    |-----------------------------------------
    | GET TOTAL REFERRED USER
    |-----------------------------------------
    */
    public function totalReferredUsers($payload){
    	// body
    	$referral 		= Referral::where("user_id", Auth::user()->id)->first();
    	$referred_users = ReferredUser::where("referral_id", $referral->id)->count();

    	$data = [
    		'total' => $referred_users
    	];

    	// return 
    	return $data;
    }
}
