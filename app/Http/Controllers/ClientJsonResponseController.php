<?php

namespace EBN\Http\Controllers;

use Illuminate\Http\Request;
use EBN\Account;
use EBN\Article;
use EBN\Referral;
use EBN\ReferredUser;
use Auth;

class ClientJsonResponseController extends Controller
{
    /*
    |-----------------------------------------
    | GET ALL ARTICLES
    |-----------------------------------------
    */
    public function fetchArticles(Request $request){
    	// body
    	$all_articles = new Article();
    	$data         = $all_articles->getAllArticlesClients($request);

    	// return response.
    	return response()->json($data);
    }

    /*
    |-----------------------------------------
    | func_desc
    |-----------------------------------------
    */
    public function fetchTrendingArticles(Request $request){
        // body
        $all_articles = new Article();
        $data         = $all_articles->getTrendingArticle();

        // return response.
        return response()->json($data);
    }

    /*
    |-----------------------------------------
    | GET USER ACCOUNT BALANCE
    |-----------------------------------------
    */
    public function getAccountBalance(Request $request){
        // body
        $request->user_id = Auth::user()->id;

        $account    = new Account();
        $data       = $account->fetchAccountByUserId($request);

        // return response.
        return response()->json($data);
    }

    /*
    |-----------------------------------------
    | GET USER ACCOUNT BALANCE
    |-----------------------------------------
    */
    public function getArticleEngagement(Request $request){
        // body
        $request->user_id = Auth::user()->id;

        $article    = new Article();
        $data       = $article->articleEngagementCount($request);

        // return response.
        return response()->json($data);
    }

    /*
    |-----------------------------------------
    | GET REFERRAL ACCOUNT
    |-----------------------------------------
    */
    public function getReferralAccount(Request $request){
        // body
        $referral   = new Referral();
        $data       = $referral->referralGetAccount($request);

        // return response.
        return response()->json($data);
    }

    /*
    |-----------------------------------------
    | GET TOTAL REFERRED USER
    |-----------------------------------------
    */
    public function getReferredUsers(Request $request){
        // body
        $referral   = new ReferredUser();
        $data       = $referral->totalReferredUsers($request);

        // return response.
        return response()->json($data);
    }
}
