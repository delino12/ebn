<?php

namespace EBN\Http\Controllers;

use Illuminate\Http\Request;
use EBN\Election;
use EBN\Voter;
use EBN\Contestant;
use Auth;

class ContestantJsonController extends Controller
{
    /*
    |-----------------------------------------
    | AUTHENTICATION
    |-----------------------------------------
    */
    public function __construct(){
    	// body
    	$this->middleware('auth:admin');
    }
    
    /*
    |-----------------------------------------
    | FETCH ALL CONTESTANT
    |-----------------------------------------
    */
    public function getAllContestants(Request $request){
    	// body
    	$contestant = new Contestant();
    	$data = $contestant->getAllContestants($request);

    	// return response.
    	return response()->json($data);
    }
    
    /*
    |-----------------------------------------
    | CREATE or STORE DATA 
    |-----------------------------------------
    */
    public function addNewContestant(Request $request){
    	// body
    	$contestant = new Contestant();
    	$data = $contestant->addNewContestant($request);

    	// return response.
    	return response()->json($data);
    }
}
