<?php

namespace EBN\Http\Controllers;

use Illuminate\Http\Request;
use EBN\Events\NotifyNewArticle;

class TestController extends Controller
{
    /*
    |-----------------------------------------
    | TEST PUSHER
    |-----------------------------------------
    */
    public function testPusher(){
    	// body
    	$event_data = [
            'id'        => rand(000, 999),
    		'title' 	=> 'Hello from dev test',
    		'message' 	=> 'This is a message from dev test',
            'avatar'    => 'https://res.cloudinary.com/delino12/image/upload/v1562252126/resume/paklkhw4lutndzz6mfmo.jpg'
    	];

    	event(new NotifyNewArticle($event_data));

    	$data = [
    		'status' 	=> 'success',
    		'message' 	=> 'Test sent!'
    	];

    	// return response.
    	return response()->json($data);
    }
}
