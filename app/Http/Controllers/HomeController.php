<?php

namespace EBN\Http\Controllers;

use Illuminate\Http\Request;
use EBN\Account;
use EBN\User;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /*
    |-----------------------------------------
    | AUTHENTICATION    
    |-----------------------------------------
    */
    public function __construct(){
        // body
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    /*
    |-----------------------------------------
    | SHOW INDEX view
    |-----------------------------------------
    */
    public function index(){
        // body
        $user = User::where('id', Auth::user()->id)->first();
        return view('home', compact('user'));
    }

    /*
    |-----------------------------------------
    | GET HOME VIEW
    |-----------------------------------------
    */
    public function profile(Request $request){
        // body
        $user = User::where('id', Auth::user()->id)->first();
        return view('profile', compact('user'));
    }
}
