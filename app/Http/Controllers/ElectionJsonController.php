<?php

namespace EBN\Http\Controllers;

use Illuminate\Http\Request;
use EBN\Election;
use EBN\Voter;
use Auth;

class ElectionJsonController extends Controller
{
    /*
    |-----------------------------------------
    | AUTHENTICATION
    |-----------------------------------------
    */
    public function __construct(){
    	// body
    	$this->middleware('auth:admin');
    }
    
    /*
    |-----------------------------------------
    | FETCH ALL ELECTIONS
    |-----------------------------------------
    */
    public function getAllElections(Request $request){
    	// body
    	$election = new Election();
    	$data = $election->getAllElections($request);

    	// return response.
    	return response()->json($data);
    }
    
    /*
    |-----------------------------------------
    | CREATE or STORE DATA 
    |-----------------------------------------
    */
    public function addNewElection(Request $request){
    	// body
    	$election = new Election();
    	$data = $election->addNewElection($request);

    	// return response.
    	return response()->json($data);
    }
    
    /*
    |-----------------------------------------
    | FETCH ONE ELECTION 
    |-----------------------------------------
    */
    public function getOneElection(Request $request){
    	// body
    	$election = new Election();
    	$data = $election->getOneElection($request);

    	// return response.
    	return response()->json($data);
    }
    
    /*
    |-----------------------------------------
    | MODIFY or UPDATE DATA 
    |-----------------------------------------
    */
    public function edit(){
    	// body
    }
    
    /*
    |-----------------------------------------
    | UPDATE DATA
    |-----------------------------------------
    */
    public function update(){
    	// body
    }
    
    /*
    |-----------------------------------------
    | DELETE DATA
    |-----------------------------------------
    */
    public function delete(){
    	// body
    }
    
    /*
    |-----------------------------------------
    | DESTROY DATA
    |-----------------------------------------
    */
    public function destroy(){
    	// body
    }
    
    
}
