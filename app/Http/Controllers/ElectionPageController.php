<?php

namespace EBN\Http\Controllers;

use Illuminate\Http\Request;

class ElectionPageController extends Controller
{
    /*
    |-----------------------------------------
    | AUTHENTICATION
    |-----------------------------------------
    */
    public function __construct(){
    	// body
    	$this->middleware('auth');
    }
    
    /*
    |-----------------------------------------
    | SHOW name VIEW INDEX
    |-----------------------------------------
    */
    public function index(){
    	// body
    	return view('election.index');
    }
    
    /*
    |-----------------------------------------
    | CREATE or STORE DATA 
    |-----------------------------------------
    */
    public function register(){
    	// body
    	return view('election.register');
    }
    
    /*
    |-----------------------------------------
    | SHOW DATA 
    |-----------------------------------------
    */
    public function results(){
    	// body
    	return view('election.results');
    }
    
    /*
    |-----------------------------------------
    | MODIFY or UPDATE DATA 
    |-----------------------------------------
    */
    public function castVote(){
    	// body
    	return view('election.votes');
    }
    
    /*
    |-----------------------------------------
    | UPDATE DATA
    |-----------------------------------------
    */
    public function update(){
    	// body
    }
    
    /*
    |-----------------------------------------
    | DELETE DATA
    |-----------------------------------------
    */
    public function delete(){
    	// body
    }
    
    /*
    |-----------------------------------------
    | DESTROY DATA
    |-----------------------------------------
    */
    public function destroy(){
    	// body
    }
    
    
}
