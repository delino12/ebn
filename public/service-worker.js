var filesToCache = [
    '/',
    'css/bootstrap.min.css',
    'css/animate.css',
    'css/default-assets/classy-nav.css',
    'css/default-assets/audioplayer.css',
    'css/owl.carousel.min.css',
    'css/magnific-popup.css',
    'css/font-awesome.min.css',
    'css/style.css',
    'https://fonts.googleapis.com/icon?family=Material+Icons',
    'https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700|Oswald:200,300,400,500,600,700',
    'js/jquery.min.js',
    'js/popper.min.js',
    'js/bootstrap.min.js',
    'js/razo.bundle.js',
    'js/default-assets/active.js',
    'js/sweetalert.min.js',
    'js/base64.js',
    'css/datatable.css',
    'js/datatable.js'
];

var staticCacheName = 'ebn-cache-v1';

self.addEventListener('install', event => {
    console.log('Attempting to install service worker and cache static assets');
    event.waitUntil(
        caches.open(staticCacheName).then(cache => {
            console.log('files has been stored in a catched!');
            return cache.addAll(filesToCache);
        })
    );
});

self.addEventListener('push', function(event) {
    console.log('Incoming push message');
    var title = 'Yay a message.';
    var body = 'We have received a push message.';
    var icon = '/img/img-core/ebn-logo.png';
    var tag = 'simple-push-demo-notification-tag';

    event.waitUntil(
        self.registration.showNotification(title, {
            body: body,
            icon: icon,
            tag: tag
        })
    );
});

self.addEventListener('fetch', function(event) {
    // console.log(event);
    // Let the browser do its default thing
    // for non-GET requests.
    if (event.request.method != 'GET') return;

    // Prevent the default, and handle the request ourselves.
    event.respondWith(async function() {
        // Try to get the response from a cache.
        const cache = await caches.open(staticCacheName);
        const cachedResponse = await cache.match(event.request);

        if (cachedResponse) {
            // If we found a match in the cache, return it, but also
            // update the entry in the cache in the background.
            event.waitUntil(cache.add(event.request));
            return cachedResponse;
        }

        // If we didn't find a match in the cache, use the network.
        return fetch(event.request);
    }());
});

self.addEventListener('message', function(event){
    if(event.data.action = 'skipWaiting'){
        self.skipWaiting();
    }
});